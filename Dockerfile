# First stage
FROM node:14 AS build

WORKDIR /build

ADD . .

RUN yarn install
RUN yarn add sharp
RUN yarn build

# Second stage
FROM nginx:stable-alpine

COPY ./docker/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build /build/dist /var/www/html
