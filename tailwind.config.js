module.exports = {
  theme: {
    // typography: {
    //   default: {
    //     css: {
    //       h1: {
    //         color: 'white'
    //       },
    //       p: {
    //         color: 'white'
    //       },
    //       em: {
    //         color: 'white'
    //       }
    //     }
    //   }
    // }
  },
  purge: [
    './src/**/*.html',
    './src/**/*.vue',
  ],
  plugins: [
    require('@tailwindcss/typography'),
  ],
}