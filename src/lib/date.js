import dayjs from 'dayjs'

export function date (value) {
  return dayjs(value).format('MMMM DD, YYYY')
}