// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

require('~/main.css')

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faBriefcase } from '@fortawesome/free-solid-svg-icons'
import {
  faGithub,
  faLinkedin,
  faDev
} from '@fortawesome/free-brands-svg-icons'

library.add(faGithub)
library.add(faLinkedin)
library.add(faBriefcase)
library.add(faDev)


import DefaultLayout from '~/layouts/Default.vue'

export default function (Vue, { router, head, isClient }) {
  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout)
  Vue.component('FontAwesome', FontAwesomeIcon)

  head.link.push({
    rel: 'preload',
    as: 'style',
    onload: "this.rel='stylesheet'",
    href: 'https://fonts.googleapis.com/css2?family=PT+Serif:wght@400;700&display=swap'
  })

  head.link.push({
    rel: 'icon',
    href: './src/favicon.ico'
  })
}
