import axios from 'axios'

const axios = axios.create({
  baseURL: process.env.GRIDSOME_API_BASE,
})
