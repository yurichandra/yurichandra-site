import axios from 'axios'

export default {
  async get (cursor) {
    try {
      const results = await axios.get(`${process.env.GRIDSOME_API_URL}/articles`, {
        params: {
          nextCursor: cursor
        }
      })

      return results.data
    } catch (error) {
      throw error
    }
  },

  async findProperty (slug) {
    try {
      const results = await axios.get(`${process.env.GRIDSOME_API_URL}/articles/properties/by-slug/${slug}`)

      return results.data
    } catch (error) {
      throw error
    }
  },

  async findContent (id) {
    try {
      const results = await axios.get(`${process.env.GRIDSOME_API_URL}/articles/content/${id}`)

      return results.data
    } catch (error) {
      throw error
    }
  }
}

